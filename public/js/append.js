 $.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return results[1] || 0;
    }
}

$("#company").val(decodeURIComponent($.urlParam('company').replace(/\+/g, ' ')));
$("#name").val(decodeURIComponent($.urlParam('name').replace(/\+/g, ' ')));
$("#email").val(decodeURIComponent($.urlParam('email')));
$("#phone").val($.urlParam('phone'));
$("#message").text(decodeURIComponent($.urlParam('package').replace(/\+/g, ' ')));